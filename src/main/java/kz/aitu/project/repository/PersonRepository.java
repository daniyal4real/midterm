package kz.aitu.project.repository;

import kz.aitu.project.entity.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
    List<Person>findAll();

    List<Person> getPersonById(long id);
}
