package kz.aitu.project.service;

import kz.aitu.project.entity.Person;
import kz.aitu.project.repository.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {
    private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    public List<Person> getPersonById(long id) {
        return personRepository.getPersonById(id);
    }

    public Person createPerson(Person person) {
        return personRepository.save(person);
    }

    public Person updatePersonById(Person person, long id) {
        return personRepository.save(person);
    }

    public void deletePersonById(long id) {
        personRepository.deleteById(id);
    }
}
