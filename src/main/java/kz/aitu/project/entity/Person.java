package kz.aitu.project.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="person")
public class Person {
    @Id
    private long id;
    private String first_name;
    private String last_name;
    private String city;
    private String phone;
    private String telegram;
}
