package kz.aitu.project.controller;

import kz.aitu.project.entity.Person;
import kz.aitu.project.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/api/v2/users/")
    public List<Person> getAllPersons(){
        return personService.getAllPersons();
    }

    @GetMapping("/api/v2/users/{id}")
    public List<Person> getPersonById(@PathVariable long id){
        return personService.getPersonById(id);
    }

    @PostMapping("/api/v2/users/")
    public ResponseEntity<?> addPerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.createPerson(person));
    }

    @PutMapping("/api/v2/users/{id}")
    public ResponseEntity<?> editPersonById(@RequestBody Person person, @PathVariable long id){
        return ResponseEntity.ok(personService.updatePersonById(person,id));
    }

    @DeleteMapping("/api/v2/users/{id}")
    public void deletePersonById(@PathVariable long id){
        personService.deletePersonById(id);
    }
}
