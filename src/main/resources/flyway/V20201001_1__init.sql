create table person(
	id int8 not null,
	first_name varchar(255),
	last_name varchar(255),
	city varchar(255),
	phone varchar(255),
	telegram varchar(255),
	primary key(id)
);